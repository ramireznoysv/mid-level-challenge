## Middle level Java Backend Developer Exercise 01

Welcome to Exercise 01!. This exercise provides a very basic POM file to start writing a Spring application.

----

### Some guidance

0. Clone this repository to your computer. Work in your clone of it, and when you're done, send us a link to your repo online
1. Use the Internet as a resource to help you complete your work, but you certainly should not try copying and pasting
2. Comment your code so that when you look back at it in a year, you'll remember what you were doing. We love well documented stuff :-)
4. Although this is a very simple challenge (should not take more than 2-3 hours of your time), please note that we are looking for skilled developers able to write testable, extensible and maintainable code
5. Have fun!

----

### The main task

You are supposed to write, test and pack a very basic core functionality for a REST-interfaced vending machine. It should work as follows

0. The machine could hold an arbitrarily large inventory, but as any vending machine, dispatch just one per request
1. The machine must be able to be resupplied while in operation
2. The machine must have three access points at minimum: `/buy`, `/supply` and `/sales`
3. `buy` access point should give access to:
    - `/buy`: list all available items in sale.
    - `/buy/item_id`: discloses the referenced item, if exists(item_id is the id for the Item object).
    - `/buy/item_id/money`: sells the referenced item if the amount of money provided is enough. Returns the item and the change after the trade(item_id is the id for the Item object and money is the amount of money to be used on purchase).
4. `supply` access point should give access to:
    - `/supply`: updates the machine inventory reading an arbitrarily large item list from a JSON array.    
5. `sales` access point should give access to:
    - `/sales`: returns a report of all historic sales with the Item name and the total money gathered with each one.
6. Each Item object must contain at least:
    - `id`: a unique Id for the object.
    - `name`: a name to describe the Item.
    - `price`: the price for every unit.
7. You can use the provided JSON file `supplies.json` as a format sample to make your tests.
8. You must provide an adequate description in case of error for each operation. As response for the request and as application log
9. You are not obligated to use any persistency mechanism; if you want to, we are very happy! Please read the next section `Bonus`
10. Do not worry about the obvious access control issues; if you are, we are very happy! Please read the next section `Bonus`

----

### Bonus

We really appreciate the proactiveness! As bonuses, you can try

1. Include some persistency layer, but keep in mind that the vending machine could use very different Item types in the future
2. Fix the access control issues including some kind of authentication. You should grant access for each path in order to support at least three user types:
    - `/buy`: can be accessed by any user
    - `/supply`: can be accessed by Operators
    - `/sales`: can be accessed by Managers